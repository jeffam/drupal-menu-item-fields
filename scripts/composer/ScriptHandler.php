<?php

/**
 * @file
 * Contains \MenuItemFields\composer\ScriptHandler.
 */

namespace MenuItemFields\composer;

use Composer\Script\Event;

class ScriptHandler
{

    /**
     * Generates a description from the README.
     */
    public static function generateDescription(Event $event)
    {
        $html = (new \Parsedown())->parse(file_get_contents('README.md'));
        // Remove first line since it is the name of the module.
        $html = preg_replace('/^.+\n/', '', $html);
        // Replace line breaks with spaces.
        $html = preg_replace('/\r|\n/', ' ', $html);
        file_put_contents('README.html', $html);
    }
}
